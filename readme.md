# Coffee Machine TDD

````

yarn
````

Pour installer jest :
````
yarn add -D jest 
````

Activer la reconnaissance des méthodes de Jest :
````
yarn add -D @types/jest 
````

Si config babel absente dans l'IDE, pour faciliter les exports/imports :
````
yarn add --dev babel-jest @babel/core @babel/preset-env  
````

Pour lancer les tests :
```
npx jest
```