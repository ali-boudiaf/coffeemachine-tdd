export function commandeBoisson(
  choixBoisson,
  quantiteSucre,
  argent = 0,
  tresChaud = false
) {
  let commandeFinale = {
    codeFinal: "",
    messageCommande: "",
  };
  let codesCommande = {
    codeBoisson: "",
    codeSucre: "",
    codeBaton: "",
    messageCommande: "",
  };

  if (quantiteSucre > 2) {
    commandeFinale.messageCommande = "Error, too much sugars !";
    return commandeFinale;
  }

  let tarif = 0;
  let argentManquant;

  switch (choixBoisson) {
    case 33:
      tarif = 0.4;
      if (argent >= tarif) {
        codesCommande.codeBoisson = "T";
        commandeFinale.messageCommande = "Drink maker makes 1 tea with ";
      } else {
        argentManquant = tarif - argent;
        commandeFinale.messageCommande =
          "Money missing: " + argentManquant.toFixed(2).toString();
        return commandeFinale;
      }
      break;
    case 34:
      tarif = 0.5;
      if (argent >= tarif) {
        codesCommande.codeBoisson = "H";
        commandeFinale.messageCommande = "Drink maker makes 1 chocolate";
      } else {
        argentManquant = tarif - argent;
        commandeFinale.messageCommande =
          "Money missing: " + argentManquant.toFixed(2).toString();
        return commandeFinale;
      }
      break;
    case 35:
      tarif = 0.6;
      if (argent >= tarif) {
        codesCommande.codeBoisson = "C";
        commandeFinale.messageCommande = "Drink maker makes 1 coffee";
      } else {
        argentManquant = tarif - argent;
        commandeFinale.messageCommande =
          "Money missing: " + argentManquant.toFixed(2).toString();
        return commandeFinale;
      }
      break;
    case 36:
      tarif = 0.6;
      if (argent >= tarif) {
        commandeFinale.codeFinal = "O::";
        commandeFinale.messageCommande = "Drink maker one orange juice";
        return commandeFinale;
      } else {
        argentManquant = tarif - argent;
        commandeFinale.messageCommande =
          "Money missing: " + argentManquant.toFixed(2).toString();
        return commandeFinale;
      }
      break;
    default:
      commandeFinale.messageCommande = "Error, please select a drink";
      return commandeFinale;
  }

  // an extra hot
  if (tresChaud) {
    commandeFinale.messageCommande += " an extra hot";
  }

  if (quantiteSucre >= 1) {
    codesCommande.codeBaton = "0";
    codesCommande.codeSucre = quantiteSucre.toString();
  }

  if (quantiteSucre == 1) {
    commandeFinale.messageCommande += " with 1 sugar and a stick";
  }

  if (quantiteSucre > 1) {
    commandeFinale.messageCommande +=
      " with " + quantiteSucre + " sugars and a stick";
  }

  if (
    quantiteSucre == 0 ||
    quantiteSucre == undefined ||
    quantiteSucre == null ||
    quantiteSucre == ""
  ) {
    commandeFinale.messageCommande += " with no sugar - and therefore no stick";
  }

  commandeFinale.codeFinal =
    codesCommande.codeBoisson +
    (tresChaud ? "h" : "") +
    ":" +
    codesCommande.codeSucre +
    ":" +
    codesCommande.codeBaton;

  return commandeFinale;
}
