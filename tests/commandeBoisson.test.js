import { commandeBoisson } from "../src/commandeBoisson.js";

describe("Configuration commande", function () {
  test("Commande chocolat et 2 sucres", function () {
    // Given
    const chocolatAvecDeuxSucres = {
      sucre: 2,
      choixBoisson: 34,
      argent: 0.5,
    };

    // When
    const result = commandeBoisson(
      chocolatAvecDeuxSucres.choixBoisson,
      chocolatAvecDeuxSucres.sucre,
      chocolatAvecDeuxSucres.argent
    );

    // Then
    expect(result).toEqual({
      codeFinal: "H:2:0",
      messageCommande:
        "Drink maker makes 1 chocolate with 2 sugars and a stick",
    });
  });

  test("Commande thé et 1 sucre", function () {
    // Given
    const theUnSucre = {
      sucre: 1,
      choixBoisson: 33,
      argent: 0.4,
    };

    // When
    const result = commandeBoisson(
      theUnSucre.choixBoisson,
      theUnSucre.sucre,
      theUnSucre.argent
    );

    // Then
    expect(result).toEqual({
      codeFinal: "T:1:0",
      messageCommande: "Drink maker makes 1 tea with  with 1 sugar and a stick",
    });
  });

  test("Commande d'un café (n°35) sans sucre", function () {
    // Given
    const cafeSansSucre = {
      sucre: 0,
      choixBoisson: 35,
      argent: 0.6,
    };

    // When
    const result = commandeBoisson(
      cafeSansSucre.choixBoisson,
      cafeSansSucre.sucre,
      cafeSansSucre.argent
    );

    // Then
    expect(result).toEqual({
      codeFinal: "C::",
      messageCommande:
        "Drink maker makes 1 coffee with no sugar - and therefore no stick",
    });
  });

  test("Commande d'une boisson avec trop de sucres", function () {
    // Given
    const chocolatAvecDeuxSucres = {
      sucre: 5,
      choixBoisson: 33,
    };

    // When
    const result = commandeBoisson(
      chocolatAvecDeuxSucres.choixBoisson,
      chocolatAvecDeuxSucres.sucre
    );

    // Then
    expect(result).toEqual({
      codeFinal: "",
      messageCommande: "Error, too much sugars !",
    });
  });

  test("Commande d'une boisson d'une boisson inexistante", function () {
    // Given
    const cafeSansSucre = {
      sucre: 0,
      choixBoisson: 42,
    };

    // When
    const result = commandeBoisson(
      cafeSansSucre.choixBoisson,
      cafeSansSucre.sucre
    );

    // Then
    expect(result).toEqual({
      codeFinal: "",
      messageCommande: "Error, please select a drink",
    });
  });

  test("Commande d'un café (35) avec sucre inexistant", function () {
    // Given
    const cafeSansSucre = {
      sucre: "",
      choixBoisson: 35,
      argent: 0.6,
    };

    // When
    const result = commandeBoisson(
      cafeSansSucre.choixBoisson,
      cafeSansSucre.sucre,
      cafeSansSucre.argent
    );

    // Then
    expect(result).toEqual({
      codeFinal: "C::",
      messageCommande:
        "Drink maker makes 1 coffee with no sugar - and therefore no stick",
    });
  });

  test("Commande d'un jus d'orange", function () {
    // Given
    const jusDOrange = {
      sucre: "",
      choixBoisson: 36,
      argent: 0.6,
    };

    // When
    const result = commandeBoisson(
      jusDOrange.choixBoisson,
      jusDOrange.sucre,
      jusDOrange.argent
    );

    // Then
    expect(result).toEqual({
      codeFinal: "O::",
      messageCommande: "Drink maker one orange juice",
    });
  });
});

describe("Gestion de l'argent", function () {
  test("Commande d'un café (n°35) avec 0,50 euros soit 10 centimes manquant", function () {
    // Given
    const cafeSansSucre = {
      sucre: "",
      choixBoisson: 35,
      argent: 0.5,
    };

    // When
    const result = commandeBoisson(
      cafeSansSucre.choixBoisson,
      cafeSansSucre.sucre,
      cafeSansSucre.argent
    );

    // Then
    expect(result).toEqual({
      codeFinal: "",
      messageCommande: "Money missing: 0.10",
    });
  });
});

describe("Gestion de la température", function () {
  test("Commande d'un café (n°35) très chaud sans sucre", function () {
    // Given
    const cafeSansSucre = {
      tresChaud: true,
      sucre: "",
      choixBoisson: 35,
      argent: 0.6,
    };

    // When
    const result = commandeBoisson(
      cafeSansSucre.choixBoisson,
      cafeSansSucre.sucre,
      cafeSansSucre.argent,
      cafeSansSucre.tresChaud
    );

    // Then
    expect(result).toEqual({
      codeFinal: "Ch::",
      messageCommande: "Drink maker makes 1 coffee an extra hot with no sugar - and therefore no stick",
    });
  });
});
